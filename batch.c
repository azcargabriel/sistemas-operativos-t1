#include <nSystem.h>
#include <fifoqueues.h>
#include "batch.h"

#include <stdio.h>

int jobThread();

struct job {
	JobFun f;
	void *params;
	void *result;
	int ready;
};

FifoQueue jobs;
nMonitor queueMonitor;
int vivo; //Indica si se ha llamado a stopBatch
int total, pendientes = 0, finalizados = 0;
nTask *taskArray;

void startBatch(int n) {
	vivo = TRUE;
	total = n;
	queueMonitor = nMakeMonitor();
	jobs = MakeFifoQueue();
	taskArray = nMalloc(n * sizeof(nTask));
	for(int i = 0; i < n; i++) {
		taskArray[i] = nEmitTask(jobThread, i);
	}
}

void stopBatch() {
	nEnter(queueMonitor);
	while(pendientes != finalizados) //No terminar mientras hayan procesos activos
		nWait(queueMonitor);
	vivo = FALSE;
	nNotifyAll(queueMonitor);
	nExit(queueMonitor);

	for(int i = 0; i < total; i++) {
		nWaitTask(taskArray[i]);
	}

	pendientes = 0;
	finalizados = 0;
}

Job *submitJob(JobFun fun, void *input) {
  Job *j = nMalloc(sizeof(*j));
  j->f = fun;
  j->params = input;
  j->ready = 0;

  nEnter(queueMonitor);  
  PutObj(jobs, j);
  pendientes++;
  nNotifyAll(queueMonitor);
  nExit(queueMonitor);

  return j;
}

void *waitJob(Job *job) {
	nEnter(queueMonitor);
	while(!job->ready) {
		nWait(queueMonitor);
	}
	nNotifyAll(queueMonitor);
	nExit(queueMonitor);

	return job->result;
}

int jobThread(int i) {
	while(vivo) {	
		nEnter(queueMonitor);
		while(EmptyFifoQueue(jobs) && vivo) {	
			nWait(queueMonitor);
		}

		//Si hubo stopBatch y no queda nada que hacer, retorno
		if(EmptyFifoQueue(jobs) && !vivo) {
			nNotifyAll(queueMonitor);
			nExit(queueMonitor);
			return 0;
		} 

		Job *j = GetObj(jobs);
		nNotifyAll(queueMonitor);
		nExit(queueMonitor);

		void* result = j->f(j->params);

		nEnter(queueMonitor);
		j->result = result;
		j->ready = 1;
		finalizados++;
		nNotifyAll(queueMonitor);
		nExit(queueMonitor);
	}

	return 0;
}
